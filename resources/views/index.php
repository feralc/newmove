<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, maximum-scale=1">
    <meta name="description" content="Agência de Publicidade e Marketing">
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="newmove, design, branding, layout, design rapido, agência de marketing, marketing, publicidade, propaganda, slogans, criar, facebook, instagram, site, website, e-commerce, loja, virtual, redes sociais, google analytics, google adwords, SEO, agência de propaganda, identidade visual, planejamento, lançamento">
    <link href="/css/app.css" rel="stylesheet" type="text/css">

    <title>New Move Marketing</title>
</head>
<body>

<div id="app" class="wrapper">

    <header class="main-header">

        <div class="container" style="position: relative;">
            <div class="brand">
                <a href="/">
                    <img src="/img/logo.png" alt="Newmove">
                </a>
            </div>
        </div>

        <div class="container">

            <div class="main-menu-wrapper">
                <button class="toggle-menu" >
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <nav class="main-menu" id="main-menu">
                    <ul>
                        <li class="item"><a href="#home">HOME</a></li>
                        <li class="item"><a href="#quem-somos">NEW MOVE</a></li>
                        <li class="item"><a href="#servicos">SERVIÇOS</a></li>
                        <li class="item"><a href="#clientes">CLIENTES</a></li>
                        <li class="item"><a href="#contato">CONTATO</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <div class="sections-wrapper">

        <section id="home" class="section">

                <div class="slider-wrapper-home">
                    <div class="home-slider slider">

                        <div class="slider-item" style="background-image: url('/img/sliders/1.png');"
                             data-mobile="/img/sliders/mobile/1.png"
                             data-desktop="/img/sliders/1.png">
                            <div class="content">
                                <div class="container">
                                    <div class="box-text">
                                       <p>
                                            KV promocional<br>
                                            para quem entende <br>
                                            muito de cães e gatos.  <br>
                                            New Ideas.
                                        </p>
                                        <div class="img-arrow"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                      <div class="slider-item" style="background-image: url('/img/sliders/2.png');"
                             data-mobile="/img/sliders/mobile/2.png"
                             data-desktop="/img/sliders/2.png">
                            <div class="content">
                                <div class="container">
                                    <div class="box-text">
                                        <p>
                                            Novidade!<br>
                                            Nova forma de<br>
                                            comprar resinas.<br>
                                            New Digital
                                        </p>
                                        <div class="img-arrow"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider-item" style="background-image: url('/img/sliders/3.png');"
                             data-mobile="/img/sliders/mobile/3.png"
                             data-desktop="/img/sliders/3.png">
                            <div class="content">
                                <div class="container">
                                    <div class="box-text">
                                        <p>
                                            A volta <br>
                                            de Eukanuba<br>
                                            ficou em nossas mãos<br>
                                            New Start
                                        </p>
                                        <div class="img-arrow"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider-item" style="background-image: url('/img/sliders/4.png');"
                             data-mobile="/img/sliders/mobile/4.png"
                             data-desktop="/img/sliders/4.png">
                            <div class="content">
                                <div class="container">
                                    <div class="box-text">
                                        <p>
                                            Redesign <br>
                                            da marca SLS com<br>
                                            o siginificado certo!<br>
                                            New Branding 
                                        </p>
                                        <div class="img-arrow"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <button class="slider-prev"></button>
                    <button class="slider-next"></button>
                </div>

        </section>

        <section id="quem-somos" class="section">

            <div class="section-title overlay-bottom">NEW MOVE</div>

            <div class="container">
                <div class="box-left">
                    <div class="slider-wrapper-quem-somos">
                        <div class="quem-somos-slider slider">
                            <div class="slider-item"><img src="/img/quem-somos/1.png"></div>
                            <div class="slider-item"><img src="/img/quem-somos/1.png"></div>
                        </div>
                        <button class="slider-prev"></button>
                        <button class="slider-next"></button>
                    </div>
                </div>

                <div class="box-right">
                    <div class="text">
                        <p>
                            A NewMove nasceu com o propósito de criar um novo conceito de agência. Temos a missão de provocar sensações além do esperado para o cliente, trazer o NOVO e ser diferente. A nossa meta é ser reconhecida como uma grande agência que saiu do estilo padrão de fazer marketing.

                        </p>

						<p>	
							Escolher a New Move não significa apenas nos contratar, mas fazer parte do nosso conceito e ter sempre novas ideias.

						</p>
						<p>
							
						</p>
						
             
                    </div>
                </div>
            </div>

            <div class="opinion">
                <div class="container">
                    <div class="flex-center">
                        <p class="title">O que os clientes falam sobre a New Move</p>
                    </div>
                    
                    <div class="slider-wrapper-opinion">
                        <div class="opinion-slider slider">
                            <div class="slider-item">
                                <p class="opinion-text">
                                    “ São inovadores mesmo. Facilitaram o nosso canal de comunicação com os clientes e agora está bem mais fácil de encontrar a Policom. ”
                                </p>
                                <p class="opinion-author">
                                    Fabiano Vitorino<br>
                                    <span>Diretor comercial - Policom</span>
                                </p>
                            </div>
                            <div class="slider-item">
                                <p class="opinion-text">
                                    “ Fazer um lançamento de um produto ou marca não é tarefa fácil, e eles conseguiram com êxito, no primeiro ano da agência. ”
                                </p>
                                <p class="opinion-author">
                                    Marcelo Rato<br>
                                    <span>Gerente Regional - Mars</span>
                                </p>
                            </div>
                            <div class="slider-item">
                                <p class="opinion-text">
                                    “ A NewMove sempre esteve ao nosso lado e foi essencial no crescimento da nossa marca nos últimos anos. ”
                                </p>
                                <p class="opinion-author">
                                    Elinho Mendonça<br>
                                    <span>Presidente - Black Blue Brasil</span>
                                </p>
                            </div>
                            <div class="slider-item">
                                <p class="opinion-text">
                                    “ Confiamos todos os anúncios e catálogos da Albasteel nas mãos da agência, porque sabemos que será um trabalho bem feito e de qualidade. ”
                                </p>
                                <p class="opinion-author">
                                    Eduardo Wetter<br>
                                    <span>Diretor Comercial - Albasteel</span>
                                </p>
                            </div>
                            <div class="slider-item">
                                <p class="opinion-text">
                                    “ Renovei toda identidade visual da minha empresa com a NewMove. E o que achei mais interessante era que tudo tinha um significado e estava relacionado com a SLS e não apenas um design bonito”
                                </p>
                                <p class="opinion-author">
                                    Sérgio Gimenez<br>
                                    <span>Diretor Executivo - SLS Áudio</span>
                                </p>
                            </div>
                            <div class="slider-item">
                                <p class="opinion-text">
                                   “ Somos uma empresa com 50 anos de mercado e nunca nossas estratégias de comunicação estiveram tão bem definidas como agora”
                                </p>
                                <p class="opinion-author">
                                    Raul Rosso <br>
                                    <span>Diretor Executivo - Jatinox</span>
                                </p>
                            </div>
                        </div>
                        <button class="slider-prev"></button>
                        <button class="slider-next"></button>
                    </div>

                </div>
            </div>

            <div class="fast-design">

                <div class="container">
                    <img src="/img/logo.png" class="newmove">

                    <div class="box-text">
                        <p>
                            A melhor<br>
                            maneira de prever<br>
                            o futuro é planejando-o.<br>
                            <b>Plan your NewMove!</b>
                            
                        </p>
                    </div>
                </div>
            </div>

        </section>

        <section id="servicos" class="section bg-dark">
            <h2 class="section-title overlay-bottom">
                SERVIÇOS<br>
                <span>Sim, somos uma agência 360!</span>
            </h2>	
            <div class="container">

                <?php
                    $services = json_decode(file_get_contents(__DIR__ . '/../../src/services.json'));
                ?>
                
                <?php foreach ($services as $serviceGroup) { ?>

                    <div class="box-service <?php echo $serviceGroup->color; ?>">
                        <div class="title">
                            <p><?php echo $serviceGroup->title; ?></p>
                        </div>
                        <ul>
                            <?php foreach ($serviceGroup->services as $service) { ?>
                                <li><a href="javascript:void(0);" data-parent="<?php echo $serviceGroup->title; ?>"><?php echo $service->title; ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                
                <?php } ?>
            </div>
        </section>

        <section id="clientes" class="section">

            <div class="section-title without-overlay">CLIENTES</div>

            <?php
                $clientes = json_decode(file_get_contents(__DIR__ . '/../../src/clientes.json'));
            ?>
            
            <div class="slider-wrapper-customers slider-grid">
                <ul class="customers-slider slider">
                    <?php foreach ($clientes as $cliente) { ?>
                        <li class="slider-item" style="background-color: <?php echo $cliente->bg; ?>; background-image: url('/img/clientes/<?php echo $cliente->img; ?>');"></li>
                    <?php } ?>
                </ul>
                <button class="slider-prev"></button>
                <button class="slider-next"></button>
            </div>

        </section>

        <section id="contato" class="section">
            <h2 class="section-title without-overlay overlay-bottom">CONTATO</h2>

            <div class="container">
                <div class="box-left">
                    <div class="row-margin"></div>
                    <div class="row-margin"></div>
                    <div class="row-margin"></div>
                    <div class="row-margin-mobile"></div>
                    <div class="row-margin-tablet"></div>
                    <p class="title">
                        Adoramos conversar!<br>
                        <span>Deixe aqui sua mensagem ou venha nos visitar.</span>
                    </p>
                    <div class="info">
                        <p class="phone">(11) 3596-3447</p>
                        <p class="email">contato@newmove.com.br</p>
                        <p class="address">
                            Rua Itapura, 249<br>
                            Tatuapé, São Paulo - SP
                        </p>
                    </div>
                    <div class="row-margin"></div>
                    <div class="row-margin"></div>
                    <div class="social">
                        <p>Acompanhe-nos nas Redes Sociais</p>
                        <a href="https://www.facebook.com/agencianewmove" target="_blank" class="facebook"></a>
                        <a href="https://www.instagram.com/agencianewmove" target="_blank" class="instagram"></a>
                    </div>
                </div>
                <div class="box-right">
                    <div class="row-margin"></div>
                    <div class="row-margin"></div>
                    <div class="row-margin-tablet"></div>

                    <div class="form-wrapper">
                        <form class="form-default" action="/submit-contact.php" method="POST" id="frmContat">

                            <div class="field"><input type="text" name="name" placeholder="Nome"></div>
                            <div class="field"><input type="email" name="email" placeholder="E-mail"></div>
                            <div class="field"><input type="text" name="phone" placeholder="Telefone" id="txtPhone"></div>
                            <div class="field"><input type="text" name="company" placeholder="Empresa"></div>
                            <div class="field">
                                <textarea name="message" placeholder="Mensagem"></textarea>
                            </div>

                            <button type="submit" class="btn-send">ENVIAR</button>

                            <p class="info"></p>

                        </form>
                    </div>

                    <div class="row-margin-tablet"></div>
                    <div class="row-margin-tablet"></div>
                    <div class="row-margin-tablet"></div>
                </div>
            </div>
            <div class="map-wrapper">
                <div id="map" class="map"></div>
            </div>
        </section>

        <div class="middle-footer hidden-xs">
            <div class="container">
                <div class="newmove">
                    <img src="/img/logo.png">
                </div>

                <div class="box address">
                    <p>
                        Endereço<br>
                        <span>Rua Itapura, 249<br>Tatuapé, São Paulo - SP</span>
                    </p>
                </div>

                <div class="multi-box">
                    <div class="box phone">
                        <p>
                            Fone: <span>(11) 3596-3447</span>
                        </p>
                    </div>

                    <div class="box email">
                        <p>
                            E-mail: <span>contato@newmove.com.br</span>
                        </p>
                    </div>
                </div>

                <div class="social">
                    <p>Acompanhe-nos<br>nas Redes Sociais</p>
                    <a href="https://www.facebook.com/agencianewmove" target="_blank" class="facebook"></a>
                    <a href="https://www.instagram.com/agencianewmove" target="_blank" class="instagram"></a>
                </div>
                
            </div>
        </div>

    </div>

    <footer>
        <div class="container">
            <p class="copyright">Copyright &copy; <?php echo date('Y') ?> - New Move</p>
            <a href="http://www.newmove.com.br" target="_blank" class="newmove">
                <img src="/img/logo.png" alt="Newmove">
            </a>
        </div>
    </footer>

</div>

<script src="/js/app.js"></script>

</body>
</html>