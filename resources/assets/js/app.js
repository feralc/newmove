
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

const app = new Vue({
    el: '#app',
    mounted: function() {

        const TABLET_WIDTH = 768;

        let $homeSlider = $('.home-slider');
        let $quemSomosSlider = $('.quem-somos-slider');
        let $opinionSlider = $('.opinion-slider');
        let $customersSlider = $('.customers-slider');

        $homeSlider.slick({
            autoplay: true,
            autoplaySpeed: 5000,
            touchMove: true,
            prevArrow: $homeSlider.parent().find('.slider-prev'),
            nextArrow: $homeSlider.parent().find('.slider-next'),
            dots: false
        });

        $quemSomosSlider.slick({
            autoplay: false,
            touchMove: true,
            prevArrow: $quemSomosSlider.parent().find('.slider-prev'),
            nextArrow: $quemSomosSlider.parent().find('.slider-next'),
            dots: false
        });

        $opinionSlider.slick({
            autoplay: false,
            touchMove: true,
            prevArrow: $opinionSlider.parent().find('.slider-prev'),
            nextArrow: $opinionSlider.parent().find('.slider-next'),
            dots: false
        });

        $customersSlider.slick({
            autoplay: true,
            autoplaySpeed: 5000,
            touchMove: true,
            prevArrow: $customersSlider.parent().find('.slider-prev'),
            nextArrow: $customersSlider.parent().find('.slider-next'),
            dots: false,
            mobileFirst: true,
            slidesToShow: 1,
            responsive: [
                {
                    breakpoint: 768,
                    settings: 'unslick'
                }
            ]
        });

        let $services = $('#servicos');
        let $overlay = $('<div class="overlay"></div>').hide();
        $services.append($overlay);
        
        $.get('/services.php', function(services) {

            
            
            
            $('.box-service').find('a').on('click', function(e) {

                let $self = $(this);
                let parent = $self.attr('data-parent');

                let service = services.filter((group) => {
                    return group.title == parent;
                })[0].services.filter((service) => {
                    return service.title == $self.text();
                })[0];

                let $box = $(`
                <div class="box">
                    <p class="title">${service.title}</p>
                    <p class="description">
                        ${service.description}
                    </p>
                </div>
            `);

                $overlay
                    .html($box)
                    .fadeIn();

                e.preventDefault();
            });


        });

        $overlay.on('click', function(e) {

            if (e.target !== e.currentTarget) {
                return;
            }

            $overlay.fadeOut();
        });

        $(window).on('resize', function() {

            $overlay
                .width($services.outerWidth())
                .height($services.outerHeight());

        }).resize();

        function showMenu(animated)
        {
            if (animated == false) {
                $('.main-menu').show();
            } else {
                $('.main-menu').stop().slideDown();
            }

            $('.toggle-menu').addClass('open');
        }

        function hideMenu(animated)
        {
            if (animated == false) {
                $('.main-menu').hide();
            } else {
                $('.main-menu').stop().slideUp();
            }

            $('.toggle-menu').removeClass('open');
        }

        function getWindowWidth()
        {
            return $(window).innerWidth();
        }

        $('#main-menu').find('ul li a').on('click', function(e) {

            let self = $(this);
            let target = self.attr('href');
            let offsetTop = $(target).offset().top;

            $('html, body').animate({scrollTop: offsetTop}, 900);

            if (getWindowWidth() <= TABLET_WIDTH) {
                hideMenu();
            }

            e.preventDefault();
        });

        $('.toggle-menu').bind('click', function () {

            var $self = $(this);

            $self.toggleClass('open');

            if ($self.hasClass('open')) {
                showMenu();
            } else {
                hideMenu();
            }

        });

        $(window).on('resize', function() {

            if ($(this).innerWidth() > TABLET_WIDTH) {

                showMenu(false);

                $homeSlider.find('.slider-item').each(function() {
                    let $slider = $(this);
                    $slider.css('background-image', 'url("' + $slider.data('desktop') + '")');
                });

            } else {
                hideMenu(false);

                $homeSlider.find('.slider-item').each(function() {
                    let $slider = $(this);
                    $slider.css('background-image', 'url("' + $slider.data('mobile') + '")');
                });

            }

        }).resize();

        $('#frmContat').submit(function(e) {

            let $form = $(this);

            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                dataType: 'json',
                data: $form.serialize(),
                success: function(response) {

                    if (! response.success) {

                        $form.find('.info').text(response.message);

                    } else {
                        $form[0].reset();

                        alert('Mensagem enviada com sucesso!');
                    }
                }
            });

            e.preventDefault();
        });

        $('#txtPhone').inputmask({
            mask: '(99) 99999-9999'
        });
        
    },

    methods: {

    }
});