import loadGoogleMapsAPI from 'load-google-maps-api';

loadGoogleMapsAPI({
    key: 'AIzaSyC6yFO35xeClEzs-uhtodmb5VgseHlaLXw'
}).then((googleMaps) => {

    let location = {lat: -23.5406308, lng: -46.5670749};

    let map = new google.maps.Map(document.getElementById('map'), {
        center: location,
        zoom: 18
    });

    let marker = new google.maps.Marker({
        position: location,
        map: map,
        title: 'BSG Advogados'
    });

    google.maps.event.addDomListener(window, "resize", function() {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });

});