<?php

require_once __DIR__ . '/../vendor/autoload.php';

$response = array('success' => true, 'message' => 'Não foi possível enviar a mensagem. Tente novamente mais tarde.');

$data = $_REQUEST;

if (! isset($data['name']) || empty($data['name'])) {

    $response = array(
        'success' => false,
        'message' => 'Digite seu nome'
    );

} elseif  (! isset($data['email']) || empty($data['email'])) {

    $response = array(
        'success' => false,
        'message' => 'Digite seu e-mail'
    );

} elseif (! filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
    $response = array(
        'success' => false,
        'message' => 'Endereço de e-mail inválido'
    );

} elseif (! isset($data['phone']) || empty($data['phone'])) {

    $response = array(
        'success' => false,
        'message' => 'Digite seu telefone'
    );

} elseif (! isset($data['message']) || empty($data['message'])) {
    $response = array(
        'success' => false,
        'message' => 'Digite a mensagem'
    );
}

if (! $response['success']) {
    header('Content-Type: application/json');
    echo json_encode($response);
    exit;
}

$response['success'] = true;

// Create the Transport
$transport = Swift_SmtpTransport::newInstance('smtp.example.org', 25)
    ->setUsername('your username')
    ->setPassword('your password')
;

$mailer = Swift_Mailer::newInstance($transport);

$body = '<b>Nome:</b> ' . $data['name'] . '<br>';
$body .= '<b>E-mail:</b> ' . $data['email'] . '<br>';
$body .= '<b>Telefone:</b> ' . $data['phone'] . '<br>';
$body .= '<b>Empresa:</b> ' . isset($data['company']) ? $data['company'] : '' . '<br>';
$body .= '<b>Mensagem:</b> ' . $data['message'] . '<br>';

$message = Swift_Message::newInstance('Newmove - Contato Website')
    ->setFrom(array('john@doe.com' => 'John Doe'))
    ->setSender(array('john@doe.com' => 'John Doe'))
    ->setTo(array('other@domain.org' => 'BSG'))
    ->setBody($body,  'text/html')
;

try {
    $response['success'] = (bool) $mailer->send($message);
} catch (Exception $e) {
    $response['success'] = false;
}

header('Content-Type: application/json');
echo json_encode($response);