<?php

$services = json_decode(file_get_contents(__DIR__ . '/../src/services.json'), true);

header('Content-Type: application/json');
echo json_encode($services);